#ifndef CAMERA_H
#define CAMERA_H

#include <vector>

#include "SDL2/SDL.h"

#include "Subject.h"
#include "Cuboid.h"
#include "Vector.h"

class Camera
{
private:
    /* data */
    static void orthographicProjection(Camera *c, float *threeDimMatrix, float *twoDimMatrix);
    float offsetX, offsetY;
    float scale;

public:
    int height, width;
    SDL_Window *window;
    SDL_Renderer *renderer;
    std::vector<Subject *> subjects;

    Camera(/* args */);
    ~Camera();

    void addSubject(Subject *subject);
    void wipe();
    void show();
};

#endif