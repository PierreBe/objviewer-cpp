#ifndef CUBOID_H
#define CUBOID_H

//#include <iostream>
//#include <math.h>
// #include <cmath>

#include "SDL2/SDL.h"

#include "Subject.h"
#include "Vector.h"

class Cuboid : public Subject
{
private:
    /* data */
public:
    // DONNEES MEMBRES STATIQUES :
    static SDL_Renderer *renderer;

    static float defaultOX;
    static float defaultOY;
    static float defaultOZ;

    static float defaultAX;
    static float defaultAY;
    static float defaultAZ;

    static float defaultDX;
    static float defaultDY;
    static float defaultDZ;
    // DONNEES MEMBRES D'INSTANCES :
    Vector vertices[8];
    Vector *edges[12][2];
    Vector origin;
    //float ax, ay, az;
    Vector orientation;
    // CONSTRUCTEURS :
    Cuboid(float ox = Cuboid::defaultOX, float oy = Cuboid::defaultOY, float oz = Cuboid::defaultOZ, float dx = Cuboid::defaultDX, float dy = Cuboid::defaultDY, float dz = Cuboid::defaultDZ, float ax = Cuboid::defaultAX, float ay = Cuboid::defaultAY, float az = Cuboid::defaultAZ);
    // DESTRUCTUCTEURS :
    ~Cuboid();
    // METHODES STATIQUES :
    static void print(Cuboid c);
    static void drawOrigin(Cuboid c);
    static void drawOrientation(Cuboid c); // ! TODO
    static void drawVertices(Cuboid c);
    static void drawEdges(Cuboid c);
    static void drawFaces(Cuboid c);

    static void translateLocal(Cuboid *c, float tx, float ty = 0, float tz = 0); // ! <cmath> NAN
    static void scaleLocal(Cuboid *c, float kx, float ky = 0, float kz = 0);
    static void rotateXLocal(Cuboid *c, float alpha);
    static void rotateYLocal(Cuboid *c, float alpha);
    static void rotateZLocal(Cuboid *c, float alpha);
    static void rotateXYZLocal(Cuboid *c, float alpha, float beta = 0, float gamma = 0);

    static void translateGlobal(Cuboid *c, float tx, float ty = 0, float tz = 0);
    static void scaleGlobal(Cuboid *c, float kx, float ky = 0, float kz = 0);
    static void rotateXGlobal(Cuboid *c, float alpha);
    static void rotateYGlobal(Cuboid *c, float alpha);
    static void rotateZGlobal(Cuboid *c, float alpha);
    static void rotateXYZGlobal(Cuboid *c, float alpha, float beta = 0, float gamma = 0);

    // METHODES D'INSTANCES :
    void print();
    void drawOrigin();
    void drawOrientation();
    void drawVertices();
    void drawEdges();
    void drawFaces();
    void draw();

    void translateLocal(float tx, float ty = 0, float tz = 0);
    void scaleLocal(float kx, float ky = 0, float kz = 0);
    void rotateXLocal(float alpha);
    void rotateYLocal(float alpha);
    void rotateZLocal(float alpha);

    void translateGlobal(float tx, float ty = 0, float tz = 0);
    void scaleGlobal(float kx, float ky = 0, float kz = 0);
    void rotateXGlobal(float alpha);
    void rotateYGlobal(float alpha);
    void rotateZGlobal(float alpha);

    void translate(float tx, float ty = 0, float tz = 0);
    void scale(float kx, float ky = 0, float kz = 0);
    void rotateX(float alpha);
    void rotateY(float alpha);
    void rotateZ(float alpha);
};

#endif