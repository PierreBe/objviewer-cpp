#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <math.h>
// #include <cmath>

#include "SDL2/SDL.h"

class Vector
{
private:
    /* data */
public:
    // DONNEES MEMBRES STATIQUES :
    static SDL_Renderer *renderer;
    static float defaultX;
    static float defaultY;
    static float defaultZ;
    static float defaultThickness;
    // DONNEES MEMBRES D'INSTANCES :
    float matrix[4];
    float thickness;
    // CONSTRUCTEURS :
    Vector(float x = Vector::defaultX, float y = Vector::defaultY, float z = Vector::defaultZ, float thickness = Vector::defaultThickness);
    // DESTRUCTUCTEURS :
    ~Vector();
    // METHODES STATIQUES :
    static void print(Vector v);
    static void drawDot(Vector v);
    static void drawSquare(Vector v);
    static void trace(Vector v);

    static void multiplyByMatrix(Vector *v, const float matrix[4][4]);
    static void translate(Vector *v, float tx, float ty = 0, float tz = 0); // ! <cmath> NAN
    static void scale(Vector *v, float kx, float ky = 0, float kz = 0);
    static void rotateX(Vector *v, float alpha);
    static void rotateY(Vector *v, float alpha);
    static void rotateZ(Vector *v, float alpha);
    static void rotateXYZ(Vector *v, float alpha, float beta, float gamma);
    static void rotateXYZ(Vector *v, Vector o);
    static void rotateZYX(Vector *v, float alpha, float beta, float gamma); //!TODO
    static void rotateZYX(Vector *v, Vector o);                             //!TODO

    static void print(Vector vs[], int size);
    static void drawDot(Vector vs[], int size);
    static void drawSquare(Vector vs[], int size);
    static void trace(Vector vs[], int size);

    static void multiplyByMatrix(Vector vs[], int size, const float matrix[4][4]);
    static void translate(Vector vs[], int size, float tx, float ty = 0, float tz = 0);
    static void scale(Vector vs[], int size, float kx, float ky = 0, float kz = 0);
    static void rotateX(Vector vs[], int size, float alpha);
    static void rotateY(Vector vs[], int size, float alpha);
    static void rotateZ(Vector vs[], int size, float alpha);
    static void rotateXYZ(Vector vs[], int size, float alpha, float beta, float gamma);
    static void rotateXYZ(Vector vs[], int size, Vector o);
    static void rotateZYX(Vector vs[], int size, float alpha, float beta, float gamma); //!TODO
    static void rotateZYX(Vector vs[], int size, Vector o);                             //!TODO

    // METHODES D'INSTANCES :
    void print();
    void drawDot();
    void drawSquare();
    void trace();
    //void move(int dx, int dy, int dz = 0);

    void multiplyByMatrix(const float matrix[4][4]);
    void translate(float tx, float ty = 0, float tz = 0);
    void scale(float kx, float ky = 0, float kz = 0);
    void rotateX(float alpha);
    void rotateY(float alpha);
    void rotateZ(float alpha);
    void rotateXYZ(float alpha, float beta, float gamma);
    void rotateXYZ(Vector o);
    void rotateZYX(float alpha, float beta, float gamma); //!TODO
    void rotateZYX(Vector o);                             //!TODO
};

#endif