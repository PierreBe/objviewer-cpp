#ifndef COORDINATESYSTEM_H
#define COORDINATESYSTEM_H

#include "Subject.h"
#include "Vector.h"

class CoordinateSystem : public Subject
{
private:
    /* data */
    SDL_Renderer *renderer;
    Vector origin;
    Vector orientation;
    int *screenHeight, *screenWidth;

public:
    CoordinateSystem(SDL_Renderer *renderer, int *screenHeight, int *screenWidth);
    ~CoordinateSystem();
    void drawOrientation();
    void draw();
};

#endif