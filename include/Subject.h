#ifndef SUBJECT_H
#define SUBJECT_H

#include "SDL2/SDL.h"

class Subject
{
public:
    virtual void draw() = 0;
};

#endif