#ifndef VERTEX_H
#define VERTEX_H

#include "SDL2/SDL.h"

#include "Vector.h"

class Vertex : public Vector
{
private:
    /* data */
public:
    static SDL_Renderer *renderer;

    SDL_Color color;
    int thickness;

    Vertex(float x, float y, float z /*, SDL_Color color = {255, 255, 255, 255}, int thickness = 10*/);
    ~Vertex();

    static void drawDot(Vertex v);
    static void drawSquare(Vertex v);
    static void trace(Vertex v);

    static void drawDot(Vertex vs[], int size);
    static void drawSquare(Vertex vs[], int size);
    static void trace(Vertex vs[], int size);

    void drawDot();
    void drawSquare();
    void trace();
};

#endif