#include <iostream>
//#include <stdlib.h>
#include <thread>
//#include <time.h>
//#include <vector>
#include <math.h>
//#define _USE_MATH_DEFINES

#include "SDL2/SDL.h"

//#include "Vector.h"
#include "Vertex.h"
#include "Cuboid.h"
#include "CoordinateSystem.h"
#include "Camera.h"

#if 0
#define CLS system("cls");
#define PRINT c.print();
#else
#define CLS
#define PRINT
#endif

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

int main(int argc, char *argv[])
{
	//int height = 600, width = 800;
	/*std::cout << "argc : " << argc << std::endl
			  << "argv : ";
	for (int i = 0; i < argc; i++)
	{
		std::cout << argv[i] << ' ';
	}
	std::cout << std::endl;*/

	//SDL_Window *window = NULL;
	//SDL_Renderer *renderer = NULL;
	SDL_Event event;
	SDL_bool done = SDL_FALSE;

	/*if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		return 1;
	}
	if (SDL_CreateWindowAndRenderer(width, height, SDL_WINDOW_RESIZABLE, &window, &renderer) != 0)
	{
		return 2;
	}

	SDL_SetWindowTitle(window, "objViewer");*/

	Camera cam = Camera();

	/*Vector::renderer = renderer;
	Cuboid::renderer = renderer;*/
	Vector::renderer = cam.renderer;
	Cuboid::renderer = cam.renderer;
	Cuboid c = Cuboid();
	CoordinateSystem r = CoordinateSystem(cam.renderer, &cam.height, &cam.width);

	cam.addSubject(&c);
	cam.addSubject(&r);

	struct st
	{
		bool up = false,
			 left = false,
			 right = false,
			 down = false,
			 forth = false,
			 back = false,
			 x = false,
			 y = false,
			 z = false,
			 sp = false,
			 sm = false,
			 f = false,
			 t = false;
	} keys;

	/*SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);*/

	//SDL_RenderPresent(renderer);

	while (!done)
	{

		cam.show();

		// FOND NOIR :
		/*SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderClear(renderer);*/

		// REPERE :
		//r.drawOrientation();

		// DESSINER LE CUBE :
		/*SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
		c.drawOrigin();
		c.drawOrientation();
		c.drawVertices();
		c.drawEdges();
		SDL_RenderPresent(renderer);*/
		CLS;
		PRINT;

		// EVENT HANDLING :
		//SDL_WaitEvent(&event);
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
			{
				bool b = event.type == SDL_KEYDOWN;
				switch (event.key.keysym.sym)
				{
				case SDLK_f:
					// ! BLOQUER LA POSITION ABSOLUE DU CURSEUR
					SDL_SetWindowFullscreen(cam.window, (keys.f ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
					keys.f = !keys.f;
					break;
				case SDLK_t:
					if (keys.t)
						Vector::rotateZYX(c.vertices, 8, c.orientation);
					else
						Vector::rotateXYZ(c.vertices, 8, c.orientation);
					keys.t = !keys.t;
					break;
				}
			}

			case SDL_KEYUP:
			{
				bool b = event.type == SDL_KEYDOWN;
				switch (event.key.keysym.sym)
				{
				case SDLK_UP:
				case SDLK_w:
					keys.up = b;
					break;
				case SDLK_LEFT:
				case SDLK_a:
					keys.left = b;
					break;
				case SDLK_RIGHT:
				case SDLK_d:
					keys.right = b;
					break;
				case SDLK_DOWN:
				case SDLK_s:
					keys.down = b;
					break;
				case SDLK_KP_DIVIDE:
					keys.back = b;
					break;
				case SDLK_KP_MULTIPLY:
					keys.forth = b;
					break;
				case SDLK_x:
					keys.x = b; // !keys.x;
					break;
				case SDLK_y:
					keys.y = b; // !keys.y;
					break;
				case SDLK_z:
					keys.z = b; // !keys.z;
					break;
				case SDLK_PLUS:
				case SDLK_KP_PLUS:
					keys.sp = b;
					break;
				case SDLK_MINUS:
				case SDLK_KP_MINUS:
					keys.sm = b;
					break;
				case SDLK_ESCAPE:
					if (b)
					{
						//delete c;
						c = Cuboid();
					}
					break;
				}
			}
			break;
			case SDL_WINDOWEVENT:
			{
				switch (event.window.event)
				{
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					//SDL_Log("Window %d size changed to %dx%d", event.window.windowID, event.window.data1, event.window.data2);
					//SDL_GetWindowSize(window, width, height);
					cam.width = event.window.data1;
					cam.height = event.window.data2;
					break;
					/*case SDL_WINDOWEVENT_RESIZED:
					SDL_Log("Window %d resized to %dx%d", event.window.windowID, event.window.data1, event.window.data2);
					break;*/
				}
			}
			break;
			case SDL_QUIT:
			{
				done = SDL_TRUE;
			}
			break;
			}
		}

		// BOUGER LE CUBE :
		if (keys.down)
			c.translate(0.0, 1);
		else if (keys.up)
			c.translate(0.0, -1);
		if (keys.left)
			c.translate(-1.0, 0);
		else if (keys.right)
			c.translate(1.0, 0);
		if (keys.forth)
			c.translate(0.0, 0, 1);
		else if (keys.back)
			c.translate(0.0, 0, -1);
		if (keys.x)
			c.rotateX(M_PI / 180);
		if (keys.y)
			c.rotateY(M_PI / 180);
		if (keys.z)
			c.rotateZ(M_PI / 180);
		if (keys.sp)
			c.scale(1 + 0.01, 1 + 0.01, 1 + 0.01);
		if (keys.sm)
			c.scale(1 - 0.01, 1 - 0.01, 1 - 0.01);

		// ATTENDRE :
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	CLS;
	//delete c;
	/*if (renderer)
	{
		SDL_DestroyRenderer(renderer);
	}
	if (window)
	{
		SDL_DestroyWindow(window);
	}*/
	cam.~Camera();
	SDL_Quit();
	return 0;
}