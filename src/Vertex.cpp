#include "Vertex.h"

SDL_Renderer *Vertex::renderer;

Vertex::Vertex(float x, float y, float z /*, SDL_Color color, int thickness*/) : Vector(x, y, z)
{
    /*this->matrix[0] = x;
    this->matrix[1] = y;
    this->matrix[2] = z;
    this->matrix[3] = 1;*/
    /*this->color = color;
    this->thickness = thickness;*/
}

Vertex::~Vertex()
{
}

void Vertex::drawDot(Vertex v)
{
    SDL_SetRenderDrawColor(Vertex::renderer, v.color.r, v.color.g, v.color.b, v.color.a);
    SDL_RenderDrawPoint(Vertex::renderer, v.matrix[0], v.matrix[1]);
}

void Vertex::drawSquare(Vertex v)
{
    SDL_Rect rect = {(int)v.matrix[0] - (int)v.thickness / 2, (int)v.matrix[1] - (int)v.thickness / 2, (int)v.thickness, (int)v.thickness};
    SDL_SetRenderDrawColor(Vertex::renderer, v.color.r, v.color.g, v.color.b, v.color.a);
    SDL_RenderFillRect(Vertex::renderer, &rect);
}

void Vertex::trace(Vertex v)
{
    SDL_SetRenderDrawColor(Vertex::renderer, v.color.r, v.color.g, v.color.b, v.color.a);
    SDL_RenderDrawLine(Vertex::renderer, 0, 0, v.matrix[0], v.matrix[1]);
}

//

void Vertex::drawDot(Vertex vs[], int size)
{
    for (int i = 0; i < size; i++)
    {
        SDL_SetRenderDrawColor(Vertex::renderer, vs[i].color.r, vs[i].color.g, vs[i].color.b, vs[i].color.a);
        Vertex::drawDot(vs[i]);
    }
}

void Vertex::drawSquare(Vertex vs[], int size)
{
    for (int i = 0; i < size; i++)
    {
        SDL_SetRenderDrawColor(Vertex::renderer, vs[i].color.r, vs[i].color.g, vs[i].color.b, vs[i].color.a);
        Vertex::drawSquare(vs[i]);
    }
}

void Vertex::trace(Vertex vs[], int size)
{
    for (int i = 0; i < size; i++)
    {
        SDL_SetRenderDrawColor(Vertex::renderer, vs[i].color.r, vs[i].color.g, vs[i].color.b, vs[i].color.a);
        Vertex::trace(vs[i]);
    }
}

//

void Vertex::drawDot()
{
    Vertex::drawDot(*this);
}

void Vertex::drawSquare()
{
    Vertex::drawSquare(*this);
}

void Vertex::trace()
{
    Vertex::trace(*this);
}