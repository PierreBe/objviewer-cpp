#include "Cuboid.h"

// DONNEES MEMBRES STATIQUES :

SDL_Renderer *Cuboid::renderer;
float Cuboid::defaultOX = 0;
float Cuboid::defaultOY = 0;
float Cuboid::defaultOZ = 0;

float Cuboid::defaultAX = 0;
float Cuboid::defaultAY = 0;
float Cuboid::defaultAZ = 0;

float Cuboid::defaultDX = 80;
float Cuboid::defaultDY = 50;
float Cuboid::defaultDZ = 30;

// CONSTRUCTEURS :

Cuboid::Cuboid(float ox, float oy, float oz, float dx, float dy, float dz, float ax, float ay, float az)
{
    this->origin = Vector(ox, oy, oz);
    this->orientation = Vector(ax, ay, az);

    this->vertices[0] = Vector(/*ox*/ 0, /*oy*/ 0 + dy, /*oz*/ 0);
    this->vertices[1] = Vector(/*ox*/ 0 + dx, /*oy*/ 0 + dy, /*oz*/ 0);
    this->vertices[2] = Vector(/*ox*/ 0 + dx, /*oy*/ 0 + dy, /*oz*/ 0 + dz);
    this->vertices[3] = Vector(/*ox*/ 0, /*oy*/ 0 + dy, /*oz*/ 0 + dz);
    this->vertices[4] = Vector(/*ox*/ 0, /*oy*/ 0, /*oz*/ 0);
    this->vertices[5] = Vector(/*ox*/ 0 + dx, /*oy*/ 0, /*oz*/ 0);
    this->vertices[6] = Vector(/*ox*/ 0 + dx, /*oy*/ 0, /*oz*/ 0 + dz);
    this->vertices[7] = Vector(/*ox*/ 0, /*oy*/ 0, /*oz*/ 0 + dz);

    for (int i = 0; i < 4; i++)
    {
        this->edges[i][0] = &this->vertices[i];
        this->edges[i][1] = &this->vertices[(i + 1) % 4];

        this->edges[i + 4][0] = &this->vertices[i + 4];
        this->edges[i + 4][1] = &this->vertices[(i + 1) % 4 + 4];

        this->edges[i + 8][0] = &this->vertices[i];
        this->edges[i + 8][1] = &this->vertices[i + 4];
    }
}

// DESTRUCTUCTEURS :

Cuboid::~Cuboid()
{
    //delete this->origin;
    /*for (Vector *v : this->vertices)
    {
        delete v;
    }*/
}

// METHODES STATIQUES :

void Cuboid::print(Cuboid c)
{
    std::cout << "origin :\t";
    Vector::print(c.origin);
    std::cout << std::endl;

    std::cout << "orientation :\t";
    Vector::print(c.orientation);
    std::cout << std::endl;

    Vector::print(c.vertices, 8);
    std::cout << std::endl;
}

void Cuboid::drawOrigin(Cuboid c)
{
    Vector::translate(c.vertices, 8, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[2]);
    SDL_SetRenderDrawColor(Cuboid::renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
    Vector::drawSquare(c.origin);
    Vector::translate(c.vertices, 8, -c.origin.matrix[0], -c.origin.matrix[1], -c.origin.matrix[2]);
}

void Cuboid::drawOrientation(Cuboid c)
{
    Vector::translate(c.vertices, 8, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[2]);
    SDL_SetRenderDrawColor(Cuboid::renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
    //
    Vector end = Vector(100, 0, 0);
    end.rotateXYZ(c.orientation);
    SDL_RenderDrawLine(Cuboid::renderer, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[0] + end.matrix[0], c.origin.matrix[1] + end.matrix[1]);
    //
    SDL_SetRenderDrawColor(Cuboid::renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
    //
    end.matrix[0] = 0;
    end.matrix[1] = 100;
    end.matrix[2] = 0;
    end.rotateXYZ(c.orientation);
    SDL_RenderDrawLine(Cuboid::renderer, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[0] + end.matrix[0], c.origin.matrix[1] + end.matrix[1]);
    //
    SDL_SetRenderDrawColor(Cuboid::renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
    //
    end.matrix[0] = 0;
    end.matrix[1] = 0;
    end.matrix[2] = 100;
    end.rotateXYZ(c.orientation);
    SDL_RenderDrawLine(Cuboid::renderer, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[0] + end.matrix[0], c.origin.matrix[1] + end.matrix[1]);
    //
    Vector::translate(c.vertices, 8, -c.origin.matrix[0], -c.origin.matrix[1], -c.origin.matrix[2]);
}

void Cuboid::drawVertices(Cuboid c)
{
    Vector::translate(c.vertices, 8, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[2]);
    SDL_SetRenderDrawColor(Cuboid::renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    Vector::drawSquare(c.vertices, 8);
    Vector::translate(c.vertices, 8, -c.origin.matrix[0], -c.origin.matrix[1], -c.origin.matrix[2]);
}

void Cuboid::drawEdges(Cuboid c)
{
    Vector::translate(c.vertices, 8, c.origin.matrix[0], c.origin.matrix[1], c.origin.matrix[2]);
    for (int i = 0; i < 4; i++)
    {
        SDL_RenderDrawLine(Cuboid::renderer, c.vertices[i].matrix[0], c.vertices[i].matrix[1], c.vertices[(i + 1) % 4].matrix[0], c.vertices[(i + 1) % 4].matrix[1]);
        SDL_RenderDrawLine(Cuboid::renderer, c.vertices[i + 4].matrix[0], c.vertices[i + 4].matrix[1], c.vertices[(i + 1) % 4 + 4].matrix[0], c.vertices[(i + 1) % 4 + 4].matrix[1]);
        SDL_RenderDrawLine(Cuboid::renderer, c.vertices[i].matrix[0], c.vertices[i].matrix[1], c.vertices[i + 4].matrix[0], c.vertices[i + 4].matrix[1]);
    }
    Vector::translate(c.vertices, 8, -c.origin.matrix[0], -c.origin.matrix[1], -c.origin.matrix[2]);
}

void Cuboid::drawFaces(Cuboid c)
{
}

void Cuboid::translateLocal(Cuboid *c, float tx, float ty, float tz)
{
    Vector::translate(c->vertices, 8, tx, ty, tz);
    //Vector::translate(c->origin, tx, ty, tz);
}

void Cuboid::scaleLocal(Cuboid *c, float kx, float ky, float kz)
{
    Vector::scale(c->vertices, 8, kx, ky, kz);
}

void Cuboid::rotateXLocal(Cuboid *c, float alpha)
{
    Vector::rotateZYX(c->vertices, 8, c->orientation);
    c->orientation.matrix[0] += alpha;
    if (c->orientation.matrix[0] > 2 * M_PI)
        c->orientation.matrix[0] -= 2 * M_PI;
    Vector::rotateXYZ(c->vertices, 8, c->orientation);
}

void Cuboid::rotateYLocal(Cuboid *c, float alpha)
{
    Vector::rotateZYX(c->vertices, 8, c->orientation);
    //Vector::rotateZ(c->vertices, 8, -c->orientation.matrix[2]);
    //Vector::rotateY(c->vertices, 8, -c->orientation.matrix[1]);
    c->orientation.matrix[1] += alpha;
    if (c->orientation.matrix[1] > 2 * M_PI)
        c->orientation.matrix[1] -= 2 * M_PI;
    Vector::rotateXYZ(c->vertices, 8, c->orientation);
    //Vector::rotateY(c->vertices, 8, c->orientation.matrix[1]);
    //Vector::rotateZ(c->vertices, 8, c->orientation.matrix[2]);
}

void Cuboid::rotateZLocal(Cuboid *c, float alpha)
{
    Vector::rotateZYX(c->vertices, 8, c->orientation);
    //Vector::rotateZ(c->vertices, 8, -c->orientation.matrix[2]);
    c->orientation.matrix[2] += alpha;
    if (c->orientation.matrix[2] > 2 * M_PI)
        c->orientation.matrix[2] -= 2 * M_PI;
    Vector::rotateXYZ(c->vertices, 8, c->orientation);
    //Vector::rotateZ(c->vertices, 8, c->orientation.matrix[2]);
}

void Cuboid::rotateXYZLocal(Cuboid *c, float alpha, float beta, float gamma)
{
    /*Vector::rotateX(c->vertices, 8, alpha);
    c->orientation->matrix[0]+=alpha;

    Vector::rotateY(c->vertices, 8, beta);
    c->orientation->matrix[1]+=beta;

    Vector::rotateZ(c->vertices, 8, gamma);
    c->orientation->matrix[2]+=gamma;*/
    Cuboid::rotateXLocal(c, alpha);
    Cuboid::rotateYLocal(c, beta);
    Cuboid::rotateZLocal(c, gamma);
}

void Cuboid::translateGlobal(Cuboid *c, float tx, float ty, float tz)
{
    Vector::translate(&c->origin, tx, ty, tz);
}

void Cuboid::scaleGlobal(Cuboid *c, float kx, float ky, float kz)
{
    Vector::scale(&c->origin, kx, ky, kz);
    Vector::scale(c->vertices, 8, kx, ky, kz);
}

void Cuboid::rotateXGlobal(Cuboid *c, float alpha)
{
    Vector::rotateX(&c->origin, alpha);
    c->orientation.matrix[0] += alpha;
    Vector::rotateX(c->vertices, 8, alpha);
}

void Cuboid::rotateYGlobal(Cuboid *c, float alpha)
{
    Vector::rotateY(&c->origin, alpha);
    c->orientation.matrix[1] += alpha;
    Vector::rotateY(c->vertices, 8, alpha);
}

void Cuboid::rotateZGlobal(Cuboid *c, float alpha)
{
    Vector::rotateZ(&c->origin, alpha);
    c->orientation.matrix[2] += alpha;
    Vector::rotateZ(c->vertices, 8, alpha);
}

void Cuboid::rotateXYZGlobal(Cuboid *c, float alpha, float beta, float gamma)
{
    /*Vector::rotateX(c->origin, alpha);
    c->orientation->matrix[0]+=alpha;
    Vector::rotateX(c->vertices, 8, alpha);

    Vector::rotateY(c->origin, beta);
    c->orientation->matrix[1]+=beta;
    Vector::rotateY(c->vertices, 8, beta);

    Vector::rotateZ(c->origin, gamma);
    c->orientation->matrix[2]+=gamma;
    Vector::rotateZ(c->vertices, 8, gamma);*/
    Cuboid::rotateXGlobal(c, alpha);
    Cuboid::rotateYGlobal(c, beta);
    Cuboid::rotateZGlobal(c, gamma);
}

// METHODES D'INSTANCES :

void Cuboid::print()
{
    Cuboid::print(*this);
}

void Cuboid::drawOrigin()
{
    Cuboid::drawOrigin(*this);
}

void Cuboid::drawOrientation()
{
    Cuboid::drawOrientation(*this);
}

void Cuboid::drawVertices()
{
    Cuboid::drawVertices(*this);
}

void Cuboid::drawEdges()
{
    Cuboid::drawEdges(*this);
}

void Cuboid::drawFaces()
{
    Cuboid::drawFaces(*this);
}

void Cuboid::draw()
{
    Cuboid::drawOrigin(*this);
    Cuboid::drawOrientation(*this);
    Cuboid::drawVertices(*this);
    //Cuboid::drawEdges(*this);
}

void Cuboid::translateLocal(float tx, float ty, float tz)
{
    Cuboid::translateLocal(this, tx, ty, tz);
}

void Cuboid::scaleLocal(float kx, float ky, float kz)
{
    Cuboid::scaleLocal(this, kx, ky, kz);
}

void Cuboid::rotateXLocal(float alpha)
{
    Cuboid::rotateXLocal(this, alpha);
}

void Cuboid::rotateYLocal(float alpha)
{
    Cuboid::rotateYLocal(this, alpha);
}

void Cuboid::rotateZLocal(float alpha)
{
    Cuboid::rotateZLocal(this, alpha);
}

void Cuboid::translateGlobal(float tx, float ty, float tz)
{
    Cuboid::translateGlobal(this, tx, ty, tz);
}

void Cuboid::scaleGlobal(float kx, float ky, float kz)
{
    Cuboid::scaleGlobal(this, kx, ky, kz);
}

void Cuboid::rotateXGlobal(float alpha)
{
    Cuboid::rotateXGlobal(this, alpha);
}

void Cuboid::rotateYGlobal(float alpha)
{
    Cuboid::rotateYGlobal(this, alpha);
}

void Cuboid::rotateZGlobal(float alpha)
{
    Cuboid::rotateZGlobal(this, alpha);
}

void Cuboid::translate(float tx, float ty, float tz)
{
    Cuboid::translateGlobal(this, tx, ty, tz);
}

void Cuboid::scale(float kx, float ky, float kz)
{
    Cuboid::scaleLocal(this, kx, ky, kz);
}

void Cuboid::rotateX(float alpha)
{
    Cuboid::rotateXLocal(this, alpha);
}

void Cuboid::rotateY(float alpha)
{
    Cuboid::rotateYLocal(this, alpha);
}

void Cuboid::rotateZ(float alpha)
{
    Cuboid::rotateZLocal(this, alpha);
}