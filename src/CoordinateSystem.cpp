#include "CoordinateSystem.h"

CoordinateSystem::CoordinateSystem(SDL_Renderer *renderer, int *screenHeight, int *screenWidth)
{
    this->renderer = renderer;
    this->screenHeight = screenHeight;
    this->screenWidth = screenWidth;
    this->origin = Vector();
    this->orientation = Vector();
}

CoordinateSystem::~CoordinateSystem()
{
}

void CoordinateSystem::drawOrientation()
{
    SDL_SetRenderDrawColor(this->renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
    Vector end = Vector(*this->screenWidth, *this->screenHeight, 0);
    SDL_RenderDrawLine(this->renderer, this->origin.matrix[0], end.matrix[1] / 2, this->origin.matrix[0] + end.matrix[0], this->origin.matrix[1] + end.matrix[1] / 2);

    SDL_SetRenderDrawColor(this->renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
    /*end.matrix[0] = 0;
    end.matrix[1] = *this->screenHeight;
    end.matrix[2] = 0;*/
    SDL_RenderDrawLine(this->renderer, end.matrix[0] / 2, this->origin.matrix[1], this->origin.matrix[0] + end.matrix[0] / 2, this->origin.matrix[1] + end.matrix[1]);

    //SDL_SetRenderDrawColor(this->renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
    /*end.matrix[0] = 0;
    end.matrix[1] = 0;
    end.matrix[2] = 0;*/
    //SDL_RenderDrawLine(this->renderer, this->origin.matrix[0], this->origin.matrix[1], this->origin.matrix[0] + end.matrix[0], this->origin.matrix[1] + end.matrix[1]);
}

void CoordinateSystem::draw()
{
    this->drawOrientation();
}