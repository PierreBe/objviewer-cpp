#include "Vector.h"

// DONNEES MEMBRES STATIQUES :

SDL_Renderer *Vector::renderer;
float Vector::defaultX = 0;
float Vector::defaultY = 0;
float Vector::defaultZ = 0;
float Vector::defaultThickness = 10;

// CONSTRUCTEURS :

Vector::Vector(float x, float y, float z, float thickness)
{
  this->matrix[0] = x;
  this->matrix[1] = y;
  this->matrix[2] = z;
  this->matrix[3] = 1;
  this->thickness = thickness;
}

// DESTRUCTUCTEURS :

Vector::~Vector()
{
}

// METHODES STATIQUES :

void Vector::print(Vector v)
{
  std::cout << "x = " << v.matrix[0] << "\t"
            << "y = " << v.matrix[1] << "\t"
            << "z = " << v.matrix[2] << "\t";
}

void Vector::drawDot(Vector v)
{
  SDL_RenderDrawPoint(Vector::renderer, v.matrix[0], v.matrix[1]);
}

void Vector::drawSquare(Vector v)
{
  //SDL_SetRenderDrawColor(Vector::renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
  SDL_Rect rect = {(int)v.matrix[0] - (int)v.thickness / 2, (int)v.matrix[1] - (int)v.thickness / 2, (int)v.thickness, (int)v.thickness};
  SDL_RenderFillRect(Vector::renderer, &rect);
}

void Vector::trace(Vector v)
{
  SDL_RenderDrawLine(Vector::renderer, 0, 0, v.matrix[0], v.matrix[1]);
}

void Vector::multiplyByMatrix(Vector *v, const float matrix[4][4])
{
  //Vector newVector = Vector(0, 0, 0, v->thickness);
  float newVector[4];
  for (int y = 0; y < 4; y++)
  {
    newVector[y] = 0;
    for (int x = 0; x < 4; x++)
    {
      /*for (int z = 0; z < 4; z++)
      {
      }*/
      //newVector.matrix[y] += matrix[y][x] * v->matrix[x];
      newVector[y] += matrix[y][x] * v->matrix[x];
    }
  }
  for (int y = 0; y < 4; y++)
  {
    //v->matrix[y] = newVector.matrix[y];
    v->matrix[y] = newVector[y];
  }
}

void Vector::translate(Vector *v, float tx, float ty, float tz)
{
  const float matrix[4][4] = {
      {1, 0, 0, tx},
      {0, 1, 0, ty},
      {0, 0, 1, tz},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(v, matrix);
}

void Vector::scale(Vector *v, float kx, float ky, float kz)
{
  const float matrix[4][4] = {
      {kx, 0, 0, 0},
      {0, ky, 0, 0},
      {0, 0, kz, 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(v, matrix);
}

void Vector::rotateX(Vector *v, float alpha)
{
  const float matrix[4][4] = {
      {1, 0, 0, 0},
      {0, cos(alpha), -sin(alpha), 0},
      {0, sin(alpha), cos(alpha), 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(v, matrix);
}

void Vector::rotateY(Vector *v, float alpha)
{
  const float matrix[4][4] = {
      {cos(alpha), 0, sin(alpha), 0},
      {0, 1, 0, 0},
      {-sin(alpha), 0, cos(alpha), 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(v, matrix);
}

void Vector::rotateZ(Vector *v, float alpha)
{
  const float matrix[4][4] = {
      {cos(alpha), -sin(alpha), 0, 0},
      {sin(alpha), cos(alpha), 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(v, matrix);
}

void Vector::rotateXYZ(Vector *v, float alpha, float beta, float gamma)
{
  Vector::rotateX(v, alpha);
  Vector::rotateY(v, beta);
  Vector::rotateZ(v, gamma);
}

void Vector::rotateXYZ(Vector *v, Vector o)
{
  Vector::rotateX(v, o.matrix[0]);
  Vector::rotateY(v, o.matrix[1]);
  Vector::rotateZ(v, o.matrix[2]);
}

//!TODO
void Vector::rotateZYX(Vector *v, float alpha, float beta, float gamma)
{
  Vector::rotateZ(v, -gamma);
  Vector::rotateY(v, -beta);
  Vector::rotateX(v, -alpha);
}

void Vector::rotateZYX(Vector *v, Vector o)
{
  Vector::rotateZ(v, -o.matrix[2]);
  Vector::rotateY(v, -o.matrix[1]);
  Vector::rotateX(v, -o.matrix[0]);
}
//!TODO

void Vector::print(Vector vs[], int size)
{
  for (int i = 0; i < size; i++) // (Vector *v : vs) // pas de foreach sur argument ?
  {
    std::cout << "vector " << i + 1 << "/" << size << " :\t";
    Vector::print(vs[i]);
    std::cout << std::endl;
  }
}

void Vector::drawDot(Vector vs[], int size)
{
  for (int i = 0; i < size; i++)
  {
    Vector::drawDot(vs[i]);
  }
}

void Vector::drawSquare(Vector vs[], int size)
{
  for (int i = 0; i < size; i++)
  {
    Vector::drawSquare(vs[i]);
  }
}

void Vector::trace(Vector vs[], int size)
{
  for (int i = 0; i < size; i++)
  {
    Vector::trace(vs[i]);
  }
}

void Vector::multiplyByMatrix(Vector vs[], int size, const float matrix[4][4])
{
  for (int i = 0; i < size; i++)
  {
    Vector::multiplyByMatrix(&vs[i], matrix);
  }
}

void Vector::translate(Vector vs[], int size, float tx, float ty, float tz)
{
  const float matrix[4][4] = {
      // ! pas cool de répéter ça 2 fois, matrixTemplate[4][4]{{0}} puis for(){for(){matrixTemplate=matrix}}
      {1, 0, 0, tx},
      {0, 1, 0, ty},
      {0, 0, 1, tz},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(vs, size, matrix);
  /*for (int i = 0; i < size; i++)
  {
    Vector::translate(vs[i], tx, ty = 0, tz = 0);
  }*/
}

void Vector::scale(Vector vs[], int size, float kx, float ky, float kz)
{
  const float matrix[4][4] = {
      {kx, 0, 0, 0},
      {0, ky, 0, 0},
      {0, 0, kz, 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(vs, size, matrix);
  /*for (int i = 0; i < size; i++)
  {
    Vector::scale(vs[i], kx, ky = 0, kz = 0);
  }*/
}

void Vector::rotateX(Vector vs[], int size, float alpha)
{
  const float matrix[4][4] = {
      {1, 0, 0, 0},
      {0, cos(alpha), -sin(alpha), 0},
      {0, sin(alpha), cos(alpha), 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(vs, size, matrix);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateX(vs[i], alpha);
  }*/
}

void Vector::rotateY(Vector vs[], int size, float alpha)
{
  const float matrix[4][4] = {
      {cos(alpha), 0, sin(alpha), 0},
      {0, 1, 0, 0},
      {-sin(alpha), 0, cos(alpha), 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(vs, size, matrix);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateY(vs[i], alpha);
  }*/
}

void Vector::rotateZ(Vector vs[], int size, float alpha)
{
  const float matrix[4][4] = {
      {cos(alpha), -sin(alpha), 0, 0},
      {sin(alpha), cos(alpha), 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
  };
  Vector::multiplyByMatrix(vs, size, matrix);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateZ(vs[i], alpha);
  }*/
}

void Vector::rotateXYZ(Vector vs[], int size, float alpha, float beta, float gamma)
{
  Vector::rotateX(vs, size, alpha);
  Vector::rotateY(vs, size, beta);
  Vector::rotateZ(vs, size, gamma);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateX(vs[i], alpha);
    Vector::rotateY(vs[i], beta);
    Vector::rotateZ(vs[i], gamma);
  }*/
}

void Vector::rotateXYZ(Vector vs[], int size, Vector o)
{
  Vector::rotateX(vs, size, o.matrix[0]);
  Vector::rotateY(vs, size, o.matrix[1]);
  Vector::rotateZ(vs, size, o.matrix[2]);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateX(vs[i], o->matrix[0]);
    Vector::rotateY(vs[i], o->matrix[1]);
    Vector::rotateZ(vs[i], o->matrix[2]);
  }*/
}

//!TODO
void Vector::rotateZYX(Vector vs[], int size, float alpha, float beta, float gamma)
{
  Vector::rotateZ(vs, size, -gamma);
  Vector::rotateY(vs, size, -beta);
  Vector::rotateX(vs, size, -alpha);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateZ(vs[i], -gamma);
    Vector::rotateY(vs[i], -beta);
    Vector::rotateX(vs[i], -alpha);
  }*/
}

void Vector::rotateZYX(Vector vs[], int size, Vector o)
{
  Vector::rotateZ(vs, size, -o.matrix[2]);
  Vector::rotateY(vs, size, -o.matrix[1]);
  Vector::rotateX(vs, size, -o.matrix[0]);
  /*for (int i = 0; i < size; i++)
  {
    Vector::rotateZ(vs[i], -o->matrix[2]);
    Vector::rotateY(vs[i], -o->matrix[1]);
    Vector::rotateX(vs[i], -o->matrix[0]);
  }*/
}
//!TODO

// METHODES D'INSTANCES :

void Vector::print()
{
  Vector::print(*this);
}

void Vector::drawDot()
{
  Vector::drawDot(*this);
}

void Vector::drawSquare()
{
  Vector::drawSquare(*this);
}

void Vector::trace()
{
  Vector::trace(*this);
}

void Vector::multiplyByMatrix(const float matrix[4][4])
{
  multiplyByMatrix(this, matrix);
}

void Vector::translate(float tx, float ty, float tz)
{
  Vector::translate(this, tx, ty, tz);
}

void Vector::scale(float kx, float ky, float kz)
{
  Vector::scale(this, kx, ky, kz);
}

void Vector::rotateX(float alpha)
{
  Vector::rotateX(this, alpha);
}

void Vector::rotateY(float alpha)
{
  Vector::rotateY(this, alpha);
}

void Vector::rotateZ(float alpha)
{
  Vector::rotateZ(this, alpha);
}

void Vector::rotateXYZ(float alpha, float beta, float gamma)
{
  Vector::rotateXYZ(this, alpha, beta, gamma);
}

void Vector::rotateXYZ(Vector o)
{
  Vector::rotateXYZ(this, o);
}

//!TODO
void Vector::rotateZYX(float alpha, float beta, float gamma)
{
  Vector::rotateZYX(this, alpha, beta, gamma);
}

void Vector::rotateZYX(Vector o)
{
  Vector::rotateZYX(this, o);
}
//!TODO

/*void Vector::move(int dx, int dy, int dz)
{
  this->matrix[0] += dx;
  this->matrix[1] += dy;
  this->matrix[2] += dz;
}*/