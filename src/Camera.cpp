#include "Camera.h"

Camera::Camera()
{
    this->height = 600;
    this->width = 800;
    this->offsetY = this->height / 2;
    this->offsetX = this->width / 2;
    this->scale = 1;
    //this->subjects = std::vector<Subject *>();
    if (!SDL_Init(SDL_INIT_VIDEO) /* != 0*/)
    {
        //return 1;
        // throw error
    }
    if (!SDL_CreateWindowAndRenderer(this->width, this->height, SDL_WINDOW_RESIZABLE, &this->window, &this->renderer) /* != 0*/)
    {
        //return 2;
        // throw error
    }

    SDL_SetWindowTitle(this->window, "objViewer");

    SDL_SetRenderDrawColor(this->renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(this->renderer);

    //SDL_RenderPresent(renderer);
}

Camera::~Camera()
{
    if (renderer)
        SDL_DestroyRenderer(renderer);
    if (window)
        SDL_DestroyWindow(window);
}

void Camera::orthographicProjection(Camera *c, float *threeDimMatrix, float *twoDimMatrix)
{
    float scaleMatrix[2][3] = {{c->scale, 0, 0},
                               {0, c->scale, 0}};
    float offsetMatrix[2] = {c->offsetX, c->offsetY};
    for (int y = 0; y < 2; y++)
    {
        twoDimMatrix[y] = 0;
        for (int x = 0; x < 3; x++)
        {
            twoDimMatrix[y] += scaleMatrix[y][x] * threeDimMatrix[x];
        }
        //twoDimMatrix[y] += offsetMatrix[y];
    }
    /*for (int i = 0; i < 2; i++) // TANT QU'ON N'A PAS DE OFFSET OU DE SCALE
    {
        twoDimMatrix[i] = threeDimMatrix[i];
    }*/
}

void Camera::addSubject(Subject *subject)
{
    this->subjects.push_back(subject);
}

void Camera::wipe()
{
    SDL_SetRenderDrawColor(this->renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(this->renderer);
}

void Camera::show()
{
    this->wipe();
    /*for (Subject *s : this->subjects)
    {
        s->draw();
    }*/
    //this->subjects[1]->draw();
    float o[2], p1[2], p2[2];
    orthographicProjection(this, ((Cuboid *)this->subjects[0])->origin.matrix, o);
    o[0] += this->offsetX;
    o[1] += this->offsetY;
    SDL_SetRenderDrawColor(this->renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    for (int i = 0; i < 12; i++)
    {
        orthographicProjection(this, ((Cuboid *)this->subjects[0])->edges[i][0]->matrix, p1);
        orthographicProjection(this, ((Cuboid *)this->subjects[0])->edges[i][1]->matrix, p2);
        SDL_RenderDrawLine(this->renderer,
                           o[0] + p1[0],
                           o[1] + p1[1],
                           o[0] + p2[0],
                           o[1] + p2[1]);
    }
    SDL_RenderPresent(this->renderer);
}