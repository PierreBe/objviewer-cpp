un programme écrit en c++ qui permet de visualiser et d'interagir avec des fichiers .obj, grâce à la bibliothèque sdl.

pour l'instant, on ne peut intéragir qu'avec un pavé, la partie lecture et interprétation de fichier n'est pas encore implémentée.
Il n'y a pas encore de gestion de la perspective.

commandes clavier, jusqu'ici :
    - ↑|w : translation axe y (global) sens +
    - ↓|s : translation axe y (global) sens -
    - ←|a : translation axe x (global) sens -
    - →|d : translation axe x (global) sens +
    - * : translation axe z (global) sens +
    - / : translation axe z (global) sens -
    - +|- : homothétie (repère local) 3 axes
    - x : rotation axe x (local) sens trigo
    - y : rotation axe y (local) sens trigo
    - z : rotation axe z (local) sens trigo
    - Esc : recharger (réinitialiser) le cube

            y↑
Repère :     ʘ →
            z  x